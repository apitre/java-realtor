#!/bin/bash

startService()
{
    if [ $(sudo systemctl is-active $1) = "active" ]
        then
            echo "service $1 already running at $2"
        else
            echo "service $1 starting at $2"
            sudo systemctl start $1
    fi
}

if [ $# = 1 ] && [ $1 = "start" ]
then
    startService "elasticsearch" "http://localhost:9200"
    startService "kibana" "http://localhost:5601"
else
    echo "banana"
fi