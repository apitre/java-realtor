package com.real.model.properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PropertyTests 
{
    @Test
    public void emptyApiResult() throws JSONException 
    {
        Property basicProperty = new RealtorProperty();

        JSONObject apiResult = new JSONObject();

        basicProperty.updateFromApiResponse(apiResult);

        assertEquals(new ArrayList<Double>(), basicProperty.getPricesHistory());
        assertEquals("", basicProperty.getId());
        assertEquals("", basicProperty.getLocation());

        assertTrue(basicProperty.getLastPriceUpdate() != "");
        assertTrue(basicProperty.getPrice() == 0);
        assertTrue(basicProperty.getMlsNumber() == 0);
        assertTrue(basicProperty.getInteriorSqft() == 0);
        assertTrue(basicProperty.getBedrooms() == 0);
        assertTrue(basicProperty.getBathrooms() == 0);
        assertTrue(basicProperty.getPricesNbUpdate() == 0);
        assertTrue(basicProperty.getKind() == 0);
    }
    
    @Test
    public void fullApiResult() throws JSONException 
    {   
        JSONObject address = new JSONObject();
        address.put("Longitude", "3333333");
        address.put("Latitude", "4444444");

        JSONObject building = new JSONObject();
        building.put("Bedrooms", "2");
        building.put("BathroomTotal", "1");
        building.put("SizeInterior", "875");
        building.put("Type", "Duplex");

        JSONObject property = new JSONObject();
        property.put("Price", "875000$");
        property.put("Address", address);

        JSONObject apiResult = new JSONObject();
        apiResult.put("Building", building);
        apiResult.put("Property", property);
        apiResult.put("MlsNumber", "123456");
        apiResult.put("PriceChangeDateUTC", "2019-07-16 19:20:30");

        Property basicProperty = new RealtorProperty();

        basicProperty.updateFromApiResponse(apiResult);

        ArrayList<Double> prices = new ArrayList<Double>();

        prices.add(875000.00);

        assertEquals(prices, basicProperty.getPricesHistory());
        assertEquals("123456", basicProperty.getId());
        assertEquals("2019-07-16 19:20:30", basicProperty.getLastPriceUpdate());
        assertEquals("4444444,3333333", basicProperty.getLocation());

        assertTrue(basicProperty.getPrice() == 875000.00);
        assertTrue(basicProperty.getMlsNumber() == 123456);
        assertTrue(basicProperty.getInteriorSqft() == 875.00);
        assertTrue(basicProperty.getBedrooms() == 2);
        assertTrue(basicProperty.getBathrooms() == 1);
        assertTrue(basicProperty.getPricesNbUpdate() == 0);
        assertTrue(basicProperty.getKind() == 3);
    }
    
    @Test
    public void priceUpdates() throws JSONException 
    {
        JSONObject property = new JSONObject();
        property.put("Price", "875000$");

        JSONObject apiResult = new JSONObject();
        apiResult.put("Property", property);

        Property basicProperty = new RealtorProperty();
        basicProperty.updateFromApiResponse(apiResult);

        property.put("Price", "850000$");
        basicProperty.updateFromApiResponse(apiResult);

        ArrayList<Double> prices = new ArrayList<Double>();

        prices.add(875000.00);
        prices.add(850000.00);

        assertEquals(prices, basicProperty.getPricesHistory());
        assertTrue(basicProperty.getPricesNbUpdate() == 1);
	}
}
