package com.real.lib;

import com.real.lib.CacheableKey;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class CacheableKeyTest
{
    @Test
    public void composeKeyString() throws NoSuchAlgorithmException
    {
        String key;

        key = CacheableKey.initialize()
                          .addParam("productids", "allo")
                          .generate();

        assertEquals("3275798294d79abb0cd0278b57654998", key);

        key = CacheableKey.initialize()
                          .addListParams("productids", Arrays.asList("banana", "alain", "test", "caca"))
                          .addParam("test", "banana")
                          .generate();

        assertEquals("6be2775b5adf5aa8234ec01e16c352b5", key);

        key = CacheableKey.initialize()
                          .addListParams("productids", Arrays.asList("banana", "test", "caca", "alain"))
                          .addListParams("anana", Arrays.asList("banana", "test", "caca", "alain"))
                          .addListParams("dog", Arrays.asList("banana", "test", "caca", "alain"))
                          .addParam("test", "banana")
                          .generate();

        assertEquals("fd2ae7b42ed0e28fb6204a49f5bb8a3e", key);

        key = CacheableKey.initialize()
                          .addParam("test", "banana")
                          .addListParams("productids", Arrays.asList("banana", "test", "caca", "alain"))
                          .addListParams("dog", Arrays.asList("banana", "test", "caca", "alain"))
                          .addListParams("anana", Arrays.asList("banana", "test", "caca", "alain"))
                          .generate();

        assertEquals("fd2ae7b42ed0e28fb6204a49f5bb8a3e", key);
	}

	@Test
    public void composeKeyInteger() throws NoSuchAlgorithmException
    {
        String key;

        key = CacheableKey.initialize()
                          .addParam("productids", 1)
                          .generate();

        assertEquals("dc88f78155b3134130c80fe59b414844", key);

        key = CacheableKey.initialize()
                          .addListParams("productids", Arrays.asList(3,4,2,1))
                          .addListParams("alain", Arrays.asList(3,10,203,1))
                          .addParam("test", 1)
                          .generate();

        assertEquals("7c1077bbaed397672472c63dca193fd2", key);

        key = CacheableKey.initialize()
                          .addListParams("alain", Arrays.asList(203,3,10,1))
                          .addParam("test", 1)
                          .addListParams("productids", Arrays.asList(1,4,2,3))
                          .generate();

	}

	@Test
    public void composeKeyMixing() throws NoSuchAlgorithmException
    {
        String key;

        key = CacheableKey.initialize()
                          .addListParams("productids", Arrays.asList("1", "2", "3", "4"))
                          .generate();

        assertEquals("e22db1b9c5438cdb8956672c0cbf13e0", key);

        key = CacheableKey.initialize()
                          .addListParams("productids", Arrays.asList(1, 2, 3, 4))
                          .generate();

        assertEquals("e22db1b9c5438cdb8956672c0cbf13e0", key);
	}
}
