package com.real.controller;

import java.util.ArrayList;

import com.real.RealApplication;
import com.real.api.curl.CurlRealtor;
import com.real.api.curl.CurlDuproprio;
import com.real.api.map.GeoLocation;
import com.real.api.map.LocationsFactory;
import com.real.repository.properties.RealtorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/test")
@Controller
public class MainController 
{
    private RealtorRepository realtorRepository;
    private LocationsFactory locationsFactory;

    @Autowired
	public MainController(RealtorRepository realtorRepository, LocationsFactory locationsFactory) {
		this.realtorRepository = realtorRepository;
		this.locationsFactory = locationsFactory;
	}

    @GetMapping("/realtor")
    public String realtor() 
    {
        ArrayList<String> names = locationsFactory.getLocationsNames();

        GeoLocation location;

        CurlRealtor curl = new CurlRealtor(this.realtorRepository);

        for (String name : names) {
            location = locationsFactory.geoLocation(name);
            System.out.println(location.getDescription());
            System.out.println("-----------------------------------------------------");
            curl.send(1, location);
        }

        return "run";
    }

    @GetMapping("/duproprio")
    public String duproprio() 
    {
        ArrayList<String> names = locationsFactory.getLocationsNames();

        GeoLocation location;

        CurlDuproprio curl = new CurlDuproprio(this.realtorRepository);

        for (String name : names) {
            location = locationsFactory.geoLocation(name);
            System.out.println(location.getDescription());
            System.out.println("-----------------------------------------------------");
            curl.send(location);
        }

        return "run";
    }
}