package com.real;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class RealApplication 
{
	public static ApplicationContext context;

	public static void main(String[] args) 
	{
		SpringApplication.run(RealApplication.class, args);
	}
}
