package com.real.model.properties;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.real.api.Price;
import com.real.api.map.GeoLocation;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.data.annotation.Id;

abstract public class Property
{
    public final static Integer KIND_UNKNOWN = 0;
    public final static Integer KIND_CONDO = 1;
    public final static Integer KIND_UNIFAMILIAL = 2;
    public final static Integer KIND_DUPLEX = 3;
    public final static Integer KIND_TRIPLEX = 4;
    public final static Integer KIND_FOURPLEX = 5;
    public final static Integer KIND_FIVEPLEX = 6;
    public final static Integer KIND_SIXPLEX = 7;
    public final static Integer KIND_TOWNHOUSE = 8;
    public final static Integer KIND_MOBILEHOUSE = 9;
    
    @Id
    protected String id;
    protected Integer mlsNumber;
    protected Integer bedrooms;
    protected Integer bathrooms;
    protected Double interiorSqft;
    protected Double price;
    protected Integer kind = 0;
    protected String location = "";
    protected String lastPriceUpdate = "";
    protected Integer pricesNbUpdate;
    protected ArrayList<Double> pricesHistory;

    protected abstract String extractLastPrice(JSONObject data);

    protected abstract GeoLocation setupGeoLocalisation(JSONObject apiResponse);

    protected abstract Integer extractType(JSONObject data);

    public void updateFromApiResponse(JSONObject apiResponse) 
    {
        try {

            JSONObject building = new JSONObject();
            
            if (apiResponse.has("Building")) {
                building = apiResponse.getJSONObject("Building");
            }

            JSONObject property = new JSONObject();
            
            if (apiResponse.has("Property")) {
                property = apiResponse.getJSONObject("Property");
            }

            String mlsNumber = apiResponse.has("MlsNumber") ? apiResponse.getString("MlsNumber") : "";
            String strPrice = property.has("Price") ? property.getString("Price") : "";

            this.id = mlsNumber;
            this.mlsNumber = mlsNumber.isEmpty() ? 0 : Integer.parseInt(mlsNumber);

            Price price = new Price(strPrice);
            GeoLocation location = this.setupGeoLocalisation(apiResponse);

            this.location = location.getGeoHash();
            this.kind = this.extractType(building);
            this.interiorSqft = this.extractSquareFeet(building);
            this.bedrooms = this.extractInteger(building, "Bedrooms", 0);
            this.bathrooms = this.extractInteger(building, "BathroomTotal", 0);
            this.price = price.toDouble();
            this.lastPriceUpdate = this.extractLastPrice(apiResponse);

            if (this.pricesHistory == null) {
                this.pricesHistory = new ArrayList<Double>();
            }

            if (this.price > 0 && this.pricesHistory.contains(this.price) == false) {
                this.pricesHistory.add(this.price);
            }

            Integer size = this.pricesHistory.size();
            
            this.pricesNbUpdate = size > 0  ? size - 1 : 0;

        } catch (JSONException e) {

            e.printStackTrace();

        }
    }

    private Integer extractInteger(JSONObject object, String key, Integer defaultValue)
    {
        try {
            return object.getInt(key);
        } catch (JSONException e) {
            return defaultValue;
        }
    }

    public String getLocation()
    {
        return this.location;
    }

    protected String now()
    {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();	    
        return dtf.format(now);  
    }

    private Double extractSquareFeet(JSONObject building) throws JSONException 
    {
        String sizeExterior = building.has("SizeInterior") ? building.getString("SizeInterior") : "";
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matches = pattern.matcher(sizeExterior);

        if (!matches.find()) {
            return 0.00;
        }

        Double value = Double.parseDouble(matches.group(0)); // second matched digits

        if (sizeExterior.indexOf("m2") != -1) {
            value = value * 10.7639;
        }

        return value;
    }

    public void updateLastPrice()
    {
        this.lastPriceUpdate = now();
    }
    
    public Double getPrice()
    {
        return this.price;
    }

    public Integer getBathrooms()
    {
        return this.bathrooms;
    }

    public Integer getMlsNumber()
    {
        return this.mlsNumber;
    } 

    public String getLastPriceUpdate()
    {
        return this.lastPriceUpdate;
    }

    public Double getInteriorSqft()
    {
       return this.interiorSqft;
    }

    public ArrayList<Double> getPricesHistory()
    {
        return this.pricesHistory;
    }

    public Integer getBedrooms()
    {
        return this.bedrooms;
    }

    public Integer getKind()
    {
        return this.kind;
    }

    public Integer getPricesNbUpdate()
    {
        return this.pricesNbUpdate;
    }

    public String getId()
    {
        return this.id;
    }

}