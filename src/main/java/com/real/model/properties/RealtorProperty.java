package com.real.model.properties;

import java.util.HashMap;
import java.util.Map;

import com.real.api.map.GeoLocation;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "buildings", type = "duproprio")
public class RealtorProperty extends Property 
{
    protected String extractLastPrice(JSONObject data)
    {
        String lastPriceUpdate = "";

        try {
            lastPriceUpdate = data.getString("PriceChangeDateUTC");
        } catch (JSONException e) {
            lastPriceUpdate = this.lastPriceUpdate;
        }

        if (lastPriceUpdate.isEmpty()) {
            return this.now();
        }

        return lastPriceUpdate;
    }

    protected Integer extractType(JSONObject data)
    {
        Map<Integer, String> kinds = new HashMap<>();

        kinds.put(Property.KIND_CONDO, "Appartement");
        kinds.put(Property.KIND_UNIFAMILIAL, "Unifamilial");
        kinds.put(Property.KIND_TOWNHOUSE, "Maison en rangée");
        kinds.put(Property.KIND_DUPLEX, "Duplex");
        kinds.put(Property.KIND_TRIPLEX, "Triplex");
        kinds.put(Property.KIND_FOURPLEX, "Quadruplex");
        kinds.put(Property.KIND_MOBILEHOUSE, "Maison mobile");

        try {
            String realtorKind = data.getString("Type");
            
            for(Map.Entry<Integer, String> kind : kinds.entrySet()){
                if (kind.getValue().equals(realtorKind)) {
                    return kind.getKey();
                }
            }

            System.out.println(realtorKind);

        } catch (JSONException e) {
            
        }

        return Property.KIND_UNKNOWN;
    }

    protected GeoLocation setupGeoLocalisation(JSONObject apiResponse) 
    {
        try {

            JSONObject address = apiResponse.getJSONObject("Property")
                                            .getJSONObject("Address");

            GeoLocation location = new GeoLocation();

            location.set(address.getString("Latitude"), address.getString("Longitude"));
            
           return location;                          

        } catch (JSONException e) {
            
            return new GeoLocation();

        }
    }
}