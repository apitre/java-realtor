package com.real.repository.properties;

import com.real.model.properties.RealtorProperty;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RealtorRepository extends ElasticsearchRepository<RealtorProperty, String>
{    
    
}