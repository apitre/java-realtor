package com.real.lib;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Util 
{
    public static String ltrim(String s) 
    {
        int i = 0;

        while (i < s.length() && Character.isWhitespace(s.charAt(i))) {
            i++;
        }

        return s.substring(i);
    }

    public static String readFile(String path, Charset encoding) throws IOException 
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
        
    public static String rtrim(String s) 
    {
        int i = s.length() - 1;
        
        while (i > 0 && Character.isWhitespace(s.charAt(i))) {
            i--;
        }

        return s.substring(0, i + 1);
    }
}
