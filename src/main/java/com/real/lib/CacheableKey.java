package com.real.lib;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

public class CacheableKey {

    private HashMap<String, List<String>> params;

    public static CacheableKey initialize() throws NoSuchAlgorithmException
    {
        return new CacheableKey(new HashMap<>());
    }

    private CacheableKey(HashMap<String, List<String>> params)
    {
        this.params = params;
    }

    public CacheableKey addListParams(String name, List<?>params)
    {
        List<String> stringList = new ArrayList<>();

        if (params != null) {
            stringList = params.stream()
                .map(String::valueOf)
                .sorted()
                .collect(Collectors.toList());
        }

        this.params.put(name, stringList);
        return this;
    }

    public CacheableKey addParam(String name, String param)
    {
        List<String> stringList = new ArrayList<>();

        if (params != null) {
            stringList = Collections.singletonList(param);
        }

        return this.addListParams(name, stringList);
    }

    public CacheableKey addParam(String key, Integer param)
    {
        return this.addParam(key, param.toString());
    }

    private List<String> keySetOrdered()
    {
        List<String> keys = new ArrayList<>(this.params.keySet());
        Collections.sort(keys);
        return keys;
    }

    private String createHash(String key) throws NoSuchAlgorithmException
    {
        byte[] messageDigest = MessageDigest.getInstance("MD5").digest(key.getBytes());

        BigInteger no = new BigInteger(1, messageDigest);

        String hash = no.toString(16);

        while (hash.length() < 32) {
            hash = "0" + hash;
        }

        return hash;
    }

    public String generate() throws NoSuchAlgorithmException
    {
        String key = "";

        for (String name : this.keySetOrdered()) {
            key += String.format("%s:[%s]", name, String.join(",", this.params.get(name)));
        }

        return this.createHash(key);
    }

}
