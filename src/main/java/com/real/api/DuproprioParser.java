package com.real.api;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DuproprioParser 
{
    public ArrayList<String> extractEntriesIds(JSONObject data)
    {
        ArrayList<String> ids = new ArrayList<String>();

        JSONArray results = new JSONArray();

        try {
            results = data.getJSONArray("data");
        } catch (JSONException e) {
            
        }

        for (int i = 0; i < results.length(); i++) {
            try {
                ids.add(results.getJSONObject(i).getString("id"));
            } catch (JSONException e) {
                
            }
        }

        return ids;
    }
}