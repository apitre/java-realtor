package com.real.api;

public class Pagination
{
    protected Integer recordsTotal = 0;
    protected Integer currentPage = 0;
    protected Integer recordsPerPage = 0;
    protected Integer totalPages = 0;

    public Integer getNextPage()
    {
        return this.currentPage + 1;
    }

    public boolean hasNext()
    {
        return this.currentPage + 1 <= this.totalPages;
    }

    public Integer getTotalRecords()
    {
        return this.recordsTotal;
    }

    public Integer getTotalRecordsPerPage()
    {
        return this.recordsPerPage;
    }

    public Integer getCurrentPage()
    {
        return this.currentPage;
    }
}