package com.real.api;

import org.json.JSONException;
import org.json.JSONObject;

public class RealtorPagination extends Pagination
{
    public RealtorPagination(JSONObject data) {
        try {
            this.recordsPerPage = data.getInt("RecordsPerPage");
            this.currentPage = data.getInt("CurrentPage");
            this.recordsTotal = data.getInt("TotalRecords");
            this.totalPages = data.getInt("TotalPages");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}