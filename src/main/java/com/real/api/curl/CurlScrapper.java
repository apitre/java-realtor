package com.real.api.curl;

import com.real.api.map.GeoLocation;

interface CurlScrapper
{
    public void send(GeoLocation location);
} 