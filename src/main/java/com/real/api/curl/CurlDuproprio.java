package com.real.api.curl;

import java.io.IOException;
import java.util.ArrayList;

import com.real.api.DuproprioParser;
import com.real.api.map.GeoLocation;
import com.real.core.curl.Curl;
import com.real.core.curl.CurlResponse;
import com.real.repository.properties.RealtorRepository;


public class CurlDuproprio
{
    private RealtorRepository realtorRepository;

    public CurlDuproprio(RealtorRepository realtorRepository)
    {
        this.realtorRepository = realtorRepository;
    }

    public void send(GeoLocation location)
    {   
        Curl curl = new Curl();

        curl.url = "https://duproprio.com/en/api-proxy/map-search";
        curl.setMethod("GET");

        curl.addParams("search_scope", "in");
        curl.addParams("max_latitude", location.getLatitudeMax());
        curl.addParams("max_longitude", location.getLongitudeMax());
        curl.addParams("min_latitude", location.getLatitudeMin());
        curl.addParams("min_longitude", location.getLongitudeMin());
        curl.addParams("province", "1");
        curl.addParams("parent", "1");
        curl.addParams("with_builders", "1");
        curl.addParams("is_for_sale", "1");

        DuproprioParser parser = new DuproprioParser();

        ArrayList<String> ids = new ArrayList<>();

        try {

            CurlResponse response = curl.send();

            ids = parser.extractEntriesIds(response.toJson());

        } catch (IOException e) {

            e.printStackTrace();

        }

        this.save(ids);

    }

    public void save(ArrayList<String> ids)
    {
        System.out.println(ids);
        /*
        for (int i = 0; i < ids.size(); i++) {
            
            try {

                String id = ids.get(i);
                RealtorProperty property = this.realtorRepository.findById(id).orElse(null);

                Curl curl = new Curl();

                curl.url = "https://duproprio.com/en/api-proxy/listing/"+id;
                curl.setMethod("GET");
                      
                try {
        
                    CurlResponse response = curl.send();
        
                    System.out.println(response.toJson().get("id"));
        
                } catch (IOException e) {
        
                    e.printStackTrace();
        
                }

        
            } catch (Exception e) {
                
                System.out.println("criss");
                
            }

        }
        */
    }
} 