package com.real.api.curl;

import java.io.IOException;

import com.real.api.Pagination;
import com.real.api.RealtorParser;
import com.real.api.map.GeoLocation;
import com.real.core.curl.Curl;
import com.real.core.curl.CurlResponse;
import com.real.model.properties.RealtorProperty;
import com.real.repository.properties.RealtorRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CurlRealtor
{
    private RealtorRepository realtorRepository;

    public CurlRealtor(RealtorRepository realtorRepository)
    {
        this.realtorRepository = realtorRepository;
    }

    public void send(Integer page, GeoLocation location)
    {   
        Curl curl = new Curl();

        curl.url = "https://api2.realtor.ca/Listing.svc/PropertySearch_Post";
        curl.setMethod("POST");

        curl.addParams("ZoomLevel", "11");
 
        curl.addParams("LatitudeMax", location.getLatitudeMax());
        curl.addParams("LongitudeMax", location.getLongitudeMax());
        curl.addParams("LatitudeMin", location.getLatitudeMin());
        curl.addParams("LongitudeMin", location.getLongitudeMin());
        curl.addParams("Sort", "6-D");
        curl.addParams("PropertyTypeGroupID", "1");
        curl.addParams("PropertySearchTypeId", "1");
        curl.addParams("TransactionTypeId", "2");
        curl.addParams("RecordsPerPage", "100");
        curl.addParams("ApplicationId", "1");
        curl.addParams("CultureId", "2");
        curl.addParams("Version", "7.0");
        curl.addParams("CurrentPage", Integer.toString(page));

        RealtorParser parser = new RealtorParser();

        System.out.println(page);

        try {

            CurlResponse response = curl.send();

            parser.load(response.toJson());

        } catch (IOException e) {

            e.printStackTrace();

        }
        
        JSONArray properties = parser.getBuildingsResults();

        Pagination pagination = parser.getPagination();

        this.save(properties);

        if (pagination.hasNext()) {
            this.send(pagination.getNextPage(), location);
        }
    }

    public void save(JSONArray properties)
    {
        for (int i = 0; i < properties.length(); i++) {
            
            try {

                JSONObject data = properties.getJSONObject(i);
                String mlsNumber = data.getString("MlsNumber");
                RealtorProperty property = this.realtorRepository.findById(mlsNumber).orElse(null);

                if (property == null) {
                    property = new RealtorProperty();
                }
    
                property.updateFromApiResponse(data);
    
                if (property.getPrice() > 0) {
                    this.realtorRepository.save(property);
                }
        
            } catch (JSONException e) {
                
                System.out.println("criss");
                
            }

        }
    }
} 