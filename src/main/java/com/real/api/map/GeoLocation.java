package com.real.api.map;

public class GeoLocation
{
    private String latitudeMax = "";
    private String longitudeMax = "";
    private String latitudeMin = "";
    private String longitudeMin = "";
    private String latitude = "";
    private String longitude = "";
    private String description = "";

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void set(String latitude, String longitude)
    {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public void setMin(String latitude, String longitude)
    {
        this.latitudeMin = latitude;
        this.longitudeMin = longitude; 
    }

    public void setMax(String latitude, String longitude)
    {
        this.latitudeMax = latitude;
        this.longitudeMax = longitude; 
    }

    public String getLatitude()
    {
        return this.latitude;
    }

    public String getLongitude()
    {
        return this.longitude;
    }

    public String getLongitudeMax()
    {
        return this.longitudeMax;
    }

    public String getLongitudeMin()
    {
        return this.longitudeMin;
    }

    public String getLatitudeMax()
    {
        return this.latitudeMax;
    }

    public String getLatitudeMin()
    {
        return this.latitudeMin;
    }

    public String getGeoHash()
    {
        if (this.latitude.isEmpty() || this.longitude.isEmpty()) {
            return "";
        }
        return this.latitude+","+this.longitude;
    }
}