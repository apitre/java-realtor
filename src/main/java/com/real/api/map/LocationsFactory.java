package com.real.api.map;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;

import com.real.core.Settings;
import com.real.lib.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LocationsFactory
{
    private Settings settings;
    private JSONObject locations;

    public LocationsFactory(Settings settings)
    {
        this.settings = settings;
        this.loadJsonLocations();
    }

    private void loadJsonLocations()
    {
        if (this.locations == null) {
            try {

                String filepath = this.settings.projectPath + "/src/main/resources/json/locations.json";
        
                String json = Util.readFile(filepath, StandardCharsets.UTF_8);
                    
                this.locations = new JSONObject(json);

            } catch (FileNotFoundException e) {
    
                e.printStackTrace();
    
            } catch (JSONException e) {
    
                e.printStackTrace();
    
            } catch (IOException e) {

                e.printStackTrace();

            }
        }
    }

    public ArrayList<String> getLocationsNames()
    {
        ArrayList<String> names = new ArrayList<>();

        Iterator<?> keys = this.locations.keys();
        
        while (keys.hasNext()) {
            names.add((String) keys.next());
        }

        return names;
    }

    public GeoLocation geoLocation(String name)
    {
        GeoLocation geoLocation = new GeoLocation();

        try {

            JSONObject location = this.locations.getJSONObject(name);
            JSONArray locationMin = location.getJSONArray("min");
            JSONArray locationMax = location.getJSONArray("max");
            
            geoLocation.setDescription(name);
            geoLocation.setMin(locationMin.getString(0), locationMin.getString(1));
            geoLocation.setMax(locationMax.getString(0), locationMax.getString(1));

            return geoLocation;

        } catch (JSONException e) {
            
            return null;

		}
    }
}