package com.real.api;

public class Price
{
    private String raw;
    private String price;

    public Price(String raw)
    {
        this.raw = raw;
        this.price = "";

        boolean isCharNumber;
        char c;
        boolean isSpace;

        for (int i = 0; i < this.raw.length(); i++ ) {
            c = this.raw.charAt(i);
            isSpace = Character.valueOf(c) == 160 || Character.isWhitespace(c);
            isCharNumber = (c >= '0' && c <= '9') || c == '.';

            if (price.length() > 0 && !isCharNumber && !isSpace) {
                break;
            }

            if (isCharNumber && !isSpace) {
                this.price += c;
            }
        }
    }

    public Double toDouble()
    {
        try {
            return Double.parseDouble(this.price);
        } catch (Exception e) {
            return 0.00;
        }
    }
}