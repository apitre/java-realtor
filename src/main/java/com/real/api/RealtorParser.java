package com.real.api;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RealtorParser 
{
    private JSONArray results;
    private Pagination pagination;

    public void load(JSONObject data) 
    {
        try {
            this.results = data.getJSONArray("Results");
            this.pagination = new RealtorPagination(data.getJSONObject("Paging"));
        } catch (JSONException e) {
            
        }
    }

    public ArrayList<String> getMlsNumbers()
    {
        ArrayList<String> ids = new ArrayList<String>();

        for (int i = 0; i < this.results.length(); i++) {
            try {
                ids.add(this.results.getJSONObject(i).getString("MlsNumber"));
            } catch (JSONException e) {
                
            }
        }

        return ids;
    }

    public Pagination getPagination()
    {
       return this.pagination;
    }

    public JSONArray getBuildingsResults() 
    {
        return this.results;
    }
}