package com.real.configuration;

import com.real.api.map.LocationsFactory;
import com.real.core.Settings;
import org.elasticsearch.common.inject.Singleton;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeansConfiguration
{
    @Bean @Singleton
    public LocationsFactory locationsFactory(Settings settings)
    {
        return new LocationsFactory(settings);
    }

     @Bean @Singleton
     public Settings settings()
     {
         return new Settings();
     }

}
