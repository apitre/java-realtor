package com.real.core;

import java.io.File;

public class Settings 
{
    public final String projectPath;

    public Settings()
    {
        this.projectPath = new File(".").getAbsolutePath().replace("/.", "");
    }
}
