package com.real.core.curl;

import com.real.core.curl.CurlResponse;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Curl 
{
    public String url;

    private String method;
    private ArrayList<String> validMethods;
    private Map<String, String> params;

    public Curl() 
    {
        this.params = new HashMap<String, String>();

        this.validMethods = new ArrayList<String>();
        this.validMethods.add("POST");
        this.validMethods.add("GET");
    }

    public void addParams(String key, String value)
    {
        this.params.put(key, value);
    }

    public void setMethod(String method) {
        String m = method.toUpperCase();

        if (this.validMethods.contains(m)) {
            this.method = m;
        }
    }

    private String sanitizeParams()
    {
        ArrayList<String> params = new ArrayList<String>();

        for (Map.Entry<String, String> param : this.params.entrySet()) {
		    params.add(param.getKey() + "=" + param.getValue());
        }
        
        return String.join("&", params);
    }

    public CurlResponse send() throws IOException
    {
        String urlParameters = this.sanitizeParams();

        URL url = new URL(this.url);

        if (this.method == "GET" && urlParameters.isEmpty() == false) {
            url = new URL(this.url+"?"+urlParameters);
        }

        HttpURLConnection con = (HttpURLConnection) url.openConnection();

		con.setRequestMethod(this.method);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");		
        con.setDoOutput(true);

        DataOutputStream wr = new DataOutputStream(con.getOutputStream());

        if (this.method == "POST") {
		    wr.writeBytes(urlParameters);
        }

        wr.flush();
		wr.close();

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
        }        
        
		in.close();
		
		return new CurlResponse(response, con.getResponseCode());
    }
}