package com.real.core.curl;

import org.json.JSONException;
import org.json.JSONObject;

public class CurlResponse 
{
    private StringBuffer response;
    private Integer statusCode;

    public CurlResponse(StringBuffer response, Integer statusCode)
    {
        this.response = response;
        this.statusCode = statusCode;
    }

    public Integer getStatusCode()
    {
        return this.statusCode;
    }

    public void jsonDebug()
    {
        try {
            System.out.println(this.toJson().toString(4));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String toString()
    {
        return this.response.toString();
    }

    public JSONObject toJson() 
    {        
        try {

            return new JSONObject(this.response.toString());

        } catch (JSONException e) {
            
            return new JSONObject();

        }
    }
}